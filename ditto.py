import argparse

import argcomplete
import getpass
import logging

from cli import ssh
from logging.handlers import TimedRotatingFileHandler
from os import environ
from pathlib import Path


class ParseKwargs(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, self.dest, dict())

        for value in values:
            key, value = value.split('=')
            getattr(namespace, self.dest)[key] = value.replace(',', '')


class Password:
    DEFAULT = 'Prompt if not specified'

    def __init__(self, value):
        if value == self.DEFAULT:
            value = getpass.getpass('Password: ')

        self.value = value

    def __str__(self):
        return self.value


def arg_parser():
    """
    required:
    -C --command
    -H --host

    optional:
    -p --password
    -i --identity_file
    -u --user
    --jump_host
    --jump_identity_file
    --jump_password
    --jump_user
    --mounts

    :return:
    """
    parser = argparse.ArgumentParser(description="Ditto parent parser")

    ## subparsers
    subparsers = parser.add_subparsers(title="actions")
    ssh_parser = subparsers.add_parser("ssh", parents=[parser], add_help=False)
    git_parser = subparsers.add_parser("git", parents=[parser], add_help=False)

    ## git args
    git_parser.add_argument('--init',
                            required=True,
                            action='store_true',
                            help='Initialize the git repo with additional files that I usually add.')

    ## ssh args
    ssh_parser.add_argument('-C,', '--command',
                            required=False,
                            action='store',
                            help='The command to run on the remote host.')
    ssh_parser.add_argument('-H,', '--host',
                            required=True,
                            action='store',
                            help='Hostname of the server. This can be an IPv4 or IPv6 address.')
    ssh_parser.add_argument('-p', '--password',
                            required=False,
                            type=Password,
                            default=environ.get('DITTO_SSH_HOST_PASSWORD'),
                            help='Password to the server. '
                                 'Default is environment variable "DITTO_SSH_HOST_PASSWORD"')
    ssh_parser.add_argument('-i', '--identity_file',
                            required=False,
                            action='store',
                            default=environ.get('DITTO_SSH_IDENTITY_FILE'),
                            help='Identity file used to connect to the server. '
                                 'Default is environment variable "DITTO_SSH_IDENTITY_FILE"')
    ssh_parser.add_argument('--jump_host',
                            required=False,
                            action='store',
                            help='Jump host name or IP.')
    ssh_parser.add_argument('--jump_identity_file',
                            required=False,
                            action='store',
                            default=environ.get('DITTO_SSH_JUMP_IDENTITY_FILE'),
                            help='Identity file used to connect to the jump server. '
                                 'Default is environment variable "DITTO_SSH_JUMP_IDENTITY_FILE"')
    ssh_parser.add_argument('--jump_password',
                            required=False,
                            type=Password,
                            default=environ.get('DITTO_SSH_JUMP_PASSWORD'),
                            help='User password for the jump host. '
                                 'Default is environment variable "DITTO_SSH_JUMP_PASSWORD"')
    ssh_parser.add_argument('--jump_user',
                            required=False,
                            action='store',
                            help='User used to connect to the jump host. '
                                 'If not defined, it will be read from the ssh config, if applicable.')
    ssh_parser.add_argument('--mount',
                            required=False,
                            action='store',
                            nargs='+',
                            help='A list of directory or file mount paths separated by a colon between the '
                                 'source and destination. Supports multiple inputs. '
                                 'Example: --mount "/tmp/foo:/etc/foo" "/var/log/test.log:/tmp/test.log"')
    ssh_parser.add_argument('-u', '--user',
                            required=False,
                            action='store',
                            help='User used to connect to the server.')

    return parser


parser = arg_parser()
argcomplete.autocomplete(parser)
args = parser.parse_args()


def log_level(level):
    """
    Allow the user to choose the logging level for the script.
    """

    thelevel = {
        "debug": logging.DEBUG,
        "info": logging.INFO,
        "warning": logging.WARNING,
        "error": logging.ERROR,
        "critical": logging.CRITICAL
    }

    return thelevel[level.lower()]


home = Path.home()
homedir = Path(f'{home}/.ditto')
if not Path.exists(homedir):
    Path.mkdir(homedir)

logsdir = Path(f'{home}/.ditto/logs')
if not Path.exists(logsdir):
    Path.mkdir(logsdir)

global_logging_level = log_level(environ.get('DITTO_LOG_LEVEL', 'warning'))

logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s',
                    level=global_logging_level,
                    handlers=[
                        logging.StreamHandler(),
                        TimedRotatingFileHandler(f'{logsdir}/ditto.log',
                                                             when='midnight', backupCount=3)])
logging.getLogger('main').setLevel(global_logging_level)


if __name__ == '__main__':
    try:
        if args.host and args.command or args.host and args.mount:
            connect = ssh.SSH().connect(hostname=args.host, command=args.command)

            if connect:
                print(connect.decode())

    except AttributeError as e:
        if 'NoneType' in str(e):
            pass

        else:
            print(f'Random error catcher: {e}')
