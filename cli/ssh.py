import ditto
import logging
import os
import paramiko
import platform
import socket

from paramiko.ssh_exception import ChannelException, SSHException
from pathlib import Path, PurePosixPath, PureWindowsPath


home = Path.home()
ssh_key = ditto.args.identity_file

logging.getLogger('paramiko').setLevel(ditto.global_logging_level)


def constructor(hostname):
    """
    Creates the ssh config based off the arguments passed in, the ssh config, or domain resolution (for hostname lookups)
    This will return a list of dictionaries that will contain the applicable settings for the ssh connection.
    """
    configs = []

    def check_dns():
        """Check DNS for the hostname"""
        try:
            _hostname = socket.gethostbyname(hostname)

            return _hostname

        except socket.error:
            return 0

    try:
        ssh_config = Path(f'{home}/.ssh/config')  # TODO - remove hardcoded value

        file = paramiko.config.SSHConfig.from_file(open(ssh_config))
        hostnames = file.get_hostnames()

        hostname_ifjump = paramiko.config.SSHConfig.from_text(f'Host {hostname}')
        cfg = file.lookup(hostname) if hostname in hostnames or check_dns() \
            else hostname_ifjump.lookup(hostname) if ditto.args.jump_host \
            else None

        if cfg is None:
            logging.error(f'"{hostname}" not found, unable to proceed!')

        main_user = ditto.args.user if ditto.args.user else cfg['user'] if 'user' in cfg else None
        identity_file = cfg['identityfile'][0] if 'identityfile' in cfg else None if ditto.args.jump_host else ssh_key

        if ditto.args.jump_host:
            jumpcfg = file.lookup(
                ditto.args.jump_host) if ditto.args.jump_host and ditto.args.jump_host in hostnames or check_dns() else None
            jump_user = ditto.args.jump_user if ditto.args.jump_user and jumpcfg['hostname'] \
                else jumpcfg['user'] if 'user' in jumpcfg and jumpcfg['hostname'] else None
            jump_identity_file = jumpcfg['identityfile'][0] if 'identityfile' in jumpcfg else ssh_key

            config = \
                {
                    'jump': {
                        'user': jump_user,
                        'hostname': jumpcfg['hostname'],
                        'identity_file': Path(jump_identity_file) if jump_identity_file is not None else None
                    },
                    'main': {
                        'user': main_user,
                        'hostname': cfg['hostname'],
                        'identity_file': Path(identity_file) if identity_file is not None else None
                    }
                }
            configs.append(config)

        else:
            config = \
                {
                    'main': {
                        'user': main_user,
                        'hostname': cfg['hostname'],
                        'identity_file': Path(identity_file) if identity_file is not None else None
                    }
                }
            configs.append(config)

        logging.info(f'The SSH configuration for this request: {config}')

        return configs

    except FileNotFoundError as e:
        logging.error(f'File not found! {e}')

    except ValueError as e:
        logging.warning(f'There was an issue with your request: {e}')


class SSH:
    def __init__(self):
        self.pssh = paramiko.SSHClient()

    ## TODO - this is littered with duplication. it needs to be cleaned up
    def mounts(self, items):
        try:
            self.sftp = self.pssh.open_sftp()

            for i in items:
                pairs = i.split(':')

                if platform.system().lower() == 'linux':
                    source, destination = Path(pairs[0]), PurePosixPath(pairs[-1])
                    logging.info(f'The source is {source}')
                    logging.info(f'The destination is {destination}')

                    if source.is_dir():
                        logging.info('Linux directory source detected.')
                        os.chdir(os.path.split(source)[0])

                        parent = os.path.split(source)[1]

                        for walker in os.walk(parent):
                            basedir = walker[0]

                            try:
                                self.sftp.chdir(os.path.join(destination, basedir))

                            except IOError:
                                self.sftp.mkdir(os.path.join(destination, basedir))

                            for dir in walker[1]:
                                recursive = f'{basedir}/{dir}'

                                try:
                                    self.sftp.chdir(os.path.join(destination, recursive))

                                except IOError:
                                    self.sftp.mkdir(os.path.join(destination, recursive))

                            for file in walker[2]:
                                self.sftp.put(os.path.join(basedir, file), os.path.join(destination, basedir, file))
                                logging.info(f'Mount successful from {os.path.join(basedir, file)} '
                                             f'to {os.path.join(destination, basedir, file)}')

                    if source.is_file():
                        logging.info('Linux file source detected.')
                        self.sftp.put(source, destination)

                elif platform.system().lower() == 'windows':
                    source, destination = Path(pairs[0]).absolute(), PureWindowsPath(pairs[-1]).as_posix()
                    logging.info(f'The source is {source}')
                    logging.info(f'The destination is {destination}')

                    if Path(pairs[0]).is_dir():
                        logging.info('Windows directory source detected.')
                        os.chdir(os.path.split(source)[0])

                        parent = os.path.split(source)[1]

                        for walker in os.walk(parent):
                            basedir = walker[0]

                            try:
                                self.sftp.chdir(PureWindowsPath(os.path.join(destination, basedir)).as_posix())
                            except IOError:
                                self.sftp.mkdir(PureWindowsPath(os.path.join(destination, basedir)).as_posix())

                            for dir in walker[1]:
                                recursive = f'{basedir}/{dir}'

                                try:
                                    self.sftp.chdir(PureWindowsPath(os.path.join(destination, recursive)).as_posix())

                                except IOError:
                                    self.sftp.mkdir(PureWindowsPath(os.path.join(destination, recursive)).as_posix())

                            for file in walker[2]:
                                self.sftp.put(
                                    PureWindowsPath(os.path.join(basedir, file)).as_posix(),
                                    PureWindowsPath(os.path.join(destination, basedir, file)).as_posix())
                                logging.info(f'Mount successful from {os.path.join(basedir, file)} '
                                             f'to {os.path.join(destination, basedir, file)}')

                    if source.is_file():
                        logging.info('Windows file source detected.')
                        self.sftp.put(
                            PureWindowsPath(source).as_posix(),
                            PureWindowsPath(destination).as_posix())

        except IOError as e:
            return f'The no home random error: {e}'

        finally:
            self.sftp.close()

    def cleanup_mount(self):
        pass

    def connect(self, **kwargs):
        """
        Opens an SSH connection to the remote host and executed the requested commands.

        :return:
        """
        config = constructor(hostname=kwargs['hostname'])

        known_hosts = Path(f'{home}/.ssh/known_hosts')  # TODO - remove the hardcoded value

        self.pssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        logging.info(f'The command executed on the host is: {kwargs["command"]}')

        try:
            self.pssh.load_host_keys(known_hosts)
            privkey = paramiko.RSAKey.from_private_key_file(config[0]['main']['identity_file']) \
                if config[0]['main']['identity_file'] is not None else None

            if ditto.args.jump_host:
                """open the jumpbox connection for use in the target ssh connection as the sock."""
                jumpbox_private_addr = config[0]['jump']['hostname']
                target_addr = config[0]['main']['hostname']
                jump_privkey = paramiko.RSAKey.from_private_key_file(config[0]['jump']['identity_file']) \
                    if config[0]['jump']['identity_file'] is not None else None

                self.jumpbox = paramiko.SSHClient()

                self.jumpbox.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                self.jumpbox.connect(hostname=jumpbox_private_addr,
                                     username=config[0]['jump']['user'],
                                     pkey=jump_privkey,
                                     password=str(ditto.args.jump_password) if ditto.args.jump_password else None)

                jumpbox_transport = self.jumpbox.get_transport()
                src_addr = (jumpbox_private_addr, 22)
                dest_addr = (target_addr, 22)

                self.jumpbox_channel = jumpbox_transport.open_channel("direct-tcpip", dest_addr, src_addr)

            """open connection to the jump host (if applicable) and target host."""
            self.pssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            self.pssh.connect(hostname=config[0]['main']['hostname'],
                              username=config[0]['main']['user'],
                              pkey=privkey,
                              password=str(ditto.args.password) if ditto.args.password else None,
                              sock=getattr(self, 'jumpbox_channel', None))

            if ditto.args.mount:
                self.mounts(ditto.args.mount)

            self.sudo = 'sudo' in kwargs['command'].split(' ') if 'command' in kwargs else None

            ## TODO - the timeout condition is ridiculous. change it to something with quicker feedback.
            ## TODO - timeout is three days. this should change.
            stdin, stdout, stderr = self.pssh.exec_command(kwargs['command'], timeout=10 if self.sudo else 259200) \
                if 'command' in kwargs else None

            stdin.write(f'{str(ditto.args.password) if ditto.args.password and self.sudo else None}\n')

            return stdout.read()

        except ChannelException as e:
            logging.error(f'Unable to connect to host! {e}')

        except FileNotFoundError as e:
            logging.error(f'Unable to find the ssh key! {e}')

        except socket.timeout as e:
            if self.sudo:
                logging.error(f'Unable to execute your "sudo" command! '
                              f'[ -p | --password <value> ] argument was not detected. '
                              f'Please include the [ -p | --password <value> ] argument when executing '
                              f'"sudo" commands! ')
            else:
                logging.critical(f'Connection timeout detected! {e}')

        except SSHException as e:
            logging.error(f'There was an issue connecting. Please check your ssh settings! - {e}')

        finally:
            self.pssh.close()
            if getattr(self, 'jumpbox', None) is not None: self.jumpbox.close()


if __name__ == '__main__':
    pass
