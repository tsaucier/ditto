# Ditto

A DevOps CLI to help with repetitive tasks I come across on frequent occasions.  

[![pipeline status](https://gitlab.com/tsaucier/ditto/badges/main/pipeline.svg)](https://gitlab.com/tsaucier/ditto/commits/main)


## Pre Requisites  
* Python >= 3.8.10
* Pip >= 21.2.4


## Getting Started  

* **Create the virtual environment:**
  ```shell
  python -m venv venv
  ```
  
  - OR -

  ```shell
  virtualenv venv
  ```

* **Activate the virtual environment:**  
  Linux:  
  ```shell
  source venv/bin/activate
  ```
  Windows:  
  ```powershell
  venv/scripts/Activate.ps1
  ```
  
* **Install the requirements:**
  ```shell
  pip install -r requirements.txt
  ```

## Environment variables  

| Name                | Value     | Description | Required | 
|:------------------- |:--------- |:----------- |:-------- |
| DITTO_LOG_LEVEL | `warning` | The logging level you want to set for the script.<br>Options are: `debug`, `info`, `warning`, `error`, and `critical` | No |


## Logging  
You can change the logging level by setting the environment variable `DITTO_LOG_LEVEL`. See [Environment variables](##Environment variables)  

Logs will be written to a file named `ditto.log` located at `$HOME/.ditto/logs/ditto.log`. This file will rotate daily at `00:00` local system time.   

## Versioning  
Versioning will be maintained in the `version.json` file located in the root of this project.

## SSH 
<details>
<summary>How to use </summary>

</details>

* **Execute the script:**  
  Single host:  
  ```shell
  python ./ditto.py \
    --host foo.example.com \
    --user sshuser \
    --identity_file ~/.ssh/id_rsa \
    --command "whoami"
  ```
  
  Jump host:  
  ```shell
  python ./ditto.py \
    --host foo.example.com \
    --user sshuser \
    --identity_file ~/.ssh/id_rsa \
    --jump_host jump.example.com \
    --jump_user jumpuser \
    --jump_identity_file ~/.ssh/jump_id_rsa \
    --command "whoami"
  ```

### Arguments and Environment Variables  

#### Arguments  

:exclamation: Default variable in all UPPERCASE are environment variables :exclamation:  

| Name               | Value          | Description                            | Required |
|:------------------ |:-------------- |:-------------------------------------- |:-------- |
| `-C`, `--command`  | No default set | The command to run on the remote host. | No      |
| `-H`, `--host`     | No default set | Hostname of the server. This can be an IPv4 or IPv6 address. | Yes |
| `-p`, `--password` | `DITTO_SSH_HOST_PASSWORD`  | Password to the server. Default is environment variable `DITTO_SSH_HOST_PASSWORD` | No | 
| `-i`, `--identity_file` | `DITTO_SSH_IDENTITY_FILE` | Identity file used to connect to the server. Default is environment variable `DITTO_SSH_IDENTITY_FILE` | No |
| `-u`, `--user`     | No default set | User used to connect to the server. | No |
| `--jump_host`      | No default set | Jump host name or IP.                  | No |
| `--jump_identity_file` | `DITTO_SSH_JUMP_IDENTITY_FILE` | Identity file used to connect to the jump server. Default is environment variable `DITTO_SSH_JUMP_IDENTITY_FILE` | No |
| `--jump_password`  | `DITTO_SSH_JUMP_PASSWORD` | User password for the jump host. Default is environment variable `DITTO_SSH_JUMP_PASSWORD` | No |
| `--jump_user`      | No default set | User used to connect to the jump host. If not defined, it will be read from the ssh config, if applicable. | No |
| `--mount`          | No default set | File or directory path to copy to the remove host.<br>Example: `--mount "/tmp/example:/var/`.

#### Environment variables  

| Name                         | Value          | Description | Required |
|:---------------------------- | :------------- | :---------- |:-------- |   
| DITTO_SSH_HOST_PASSWORD      | No default set | Password to the host you are ssh-ing to, if applicable. | No |
| DITTO_SSH_IDENTITY_FILE      | No default set | Identity file used to connect to the ssh-able. | No | 
| DITTO_SSH_JUMP_IDENTITY_FILE | No default set | Identity file used to connect to the ssh-able jump host. | No |
| DITTO_SSH_JUMP_PASSWORD      | No default set | Password to the jump host you are ssh-ing to, if applicable. | No |
